import Circle from '../basicShapes/Circle'

export default class Snow extends Circle {


  constructor(x, y, r, vy, vx, color="blue", outlineColor="transparent") {
    super(x, y, r, color)
    this.vx = vx
    this.vy = vy
  }

  tick(context) {
    const {canvas} = global.game._context
    this.y = (this.y + this.vy) % canvas.clientHeight
    this.x = (this.x + this.vx) % canvas.clientWidth
  }
}
