import React, { Component } from 'react';
import Game from './Game'

export default class TowerDefence extends Component {
  render() {
    return (
      <div className='grid-wrapper'>
        <h1 className='title'>2018 Capstone Game</h1>
        
        <div className='game-Picker'>
       <a href="/"><button>home</button></a>
        </div>
        
        <div className= 'game-box'>
        <div className='lazerTower1'></div>
        </div>
        
        <div className="game-wrapper">
          <Game className="game-box"/>

        </div>

      </div>
    );
  }
}
//made by Joshua


