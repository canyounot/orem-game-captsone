import React, { Component } from 'react';
import GameDescription from './gamedescription';


export default class App extends Component {
  render() {

    const gameTitles = ["Tower Defense", "Slime Ball", "Star Wars Jedi Man"]

    const gameDescriptions = [];
    for (let i = 0; i < gameTitles.length; i++) {
      const gameTitle = gameTitles[i];
      gameDescriptions.push(<li><GameDescription title={gameTitle}/></li>);
    }

    return (
      <div className='grid-wrapper'>
        <h1 className='title'>2018 Capstone Game Website</h1>

        
        <div className='game-Picker'>
          <a href="towerdefence"><button>tower defence</button></a>
        </div>

      <div className='new-games-container'> 
        <h2>NEW GAMES</h2>
        <ul>
          {gameDescriptions}
        </ul>
      </div>
      
      </div>
    );
  }
}
