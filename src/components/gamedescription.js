import React, { Component } from 'react';

export default class GameDescription extends Component {
  render () {
    return (
      <div className="game-description">
        <img src="http://via.placeholder.com/95x62"></img>
        <div>
          <h3>{this.props.title}</h3>
          <p> Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ut, ad omnis. Quo unde voluptatibus voluptate, doloribus distinctio quia voluptas dignissimos iste sint veritatis accusantium eaque dolore ipsa omnis animi magni. </p>
        </div>
      </div>
    );
  }
}