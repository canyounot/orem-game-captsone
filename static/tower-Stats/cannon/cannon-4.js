const cannonTower4 = cannonTower4;
    
    cannonTower4.dmg = 18;
    cannonTower4.rof = 4;
    cannonTower4.size = 2;
    cannonTower4.range = 10;

    cannonTower4.upgradeCost = 200;

    cannonTower4.upgradeText = "upgrades to Altilery station";


export default cannonTower4