const hyperTower4 = hyperTower4;
    
    hyperTower4.dmg = 5;
    hyperTower4.rof = 16;
    hyperTower4.size = 3;
    hyperTower4.range = 9;
    hyperTower4.upgradeCost = 300;

    hyperTower4.upgradeText = "Upgrade to anaihalator beam";


export default hyperTower4;